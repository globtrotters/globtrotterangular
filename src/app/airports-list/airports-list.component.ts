import {Component, OnInit} from '@angular/core';
import {Airport} from '../shared/model/airport';
import {AirportService} from '../shared/service/airport.service';
import {Country} from '../shared/model/country';

@Component({
  selector: 'app-airports-list',
  templateUrl: './airports-list.component.html',
  styleUrls: ['./airports-list.component.css']
})
export class AirportsListComponent implements OnInit {

  private airports: Airport[];
  private airport: Airport = new Airport();

  constructor(private airportService: AirportService) {
  }

  ngOnInit() {
    this.airportService.getAirports().subscribe(value => {
      console.log(value);
      this.airports = value;
    });
  }

  onDelete(id: number): void {
    this.airportService.delete(id);
  }

}
