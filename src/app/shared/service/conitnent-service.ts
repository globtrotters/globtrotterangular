import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Continent} from '../model/continent';

@Injectable({
  providedIn: 'root'
})
export class ContinentServiceService {
  constructor(private http: HttpClient) {
  }

  getContinents(): Observable<Continent[]> {
    return this.http.get<Continent[]>('http://localhost:7070/api/continent/all');
  }

  getContinent(id: number): Observable<Continent> {
    return this.http.get<Continent>('http://localhost:7070/api/continent/findById/' + id);
  }

  save(continent: Continent): void {
    if (continent.id == null) {
      this.http.post('http://localhost:7070/api/continent', continent).subscribe(value => {
        console.log(value);
        window.location.href = '/continent-list';
      });
    } else {
      this.http.put('http://localhost:7070/api/continent', continent).subscribe(value => {
        console.log(value);
        window.location.href = '/continent-list';
      });
    }
  }

  delete(id: number): void {
    this.http.delete('http://localhost:7070/api/continent/delete/' + id).subscribe(value => {
      console.log(value);
      window.location.href = '/continent-list';
    });
  }

}
