import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {City} from '../model/city';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(private http: HttpClient) {
  }

  getCities(): Observable<City[]> {
    return this.http.get<City[]>('http://localhost:7070/api/city/all');
  }

  getCity(id: number): Observable<City> {
    return this.http.get<City>('http://localhost:7070/api/city/findById/' + id);
  }

  save(city: City): void {
    if (city.id == null) {
      this.http.post('http://localhost:7070/api/city', city).subscribe(value => {
        console.log(value);
        window.location.href = '/city-list';
      });
    } else {
      this.http.put('http://localhost:7070/api/city', city).subscribe(value => {
        console.log(value);
        window.location.href = '/city-list';
      });
    }
  }

  delete(id: number): void {
    this.http.delete('http://localhost:7070/api/city/delete/' + id).subscribe(value => {
      console.log(value);
      window.location.href = '/city-list';
    });
  }

  findByCountry(countryName: string): Observable<City[]> {
    return this.http.get<City[]>('http://localhost:7070/api/city/findByCountry?name=' + countryName);
  }
}
