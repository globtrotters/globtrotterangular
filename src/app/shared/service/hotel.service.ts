import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Hotel} from '../model/hotel';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private http: HttpClient) {
  }

  getHotels(): Observable<Hotel[]> {
    return this.http.get<Hotel[]>('http://localhost:7070//api/hotel/all');

  }

  getHotel(id: number): Observable<Hotel> {
    return this.http.get<Hotel>('http://localhost:7070/api/hotel/findById/' + id);
  }

  save(hotel: Hotel): void {
    if (hotel.id == null) {
      this.http.post('http://localhost:7070/api/hotel', hotel).subscribe(value => {
        console.log(value);
        window.location.href = '/hotels-list';
      });
    } else {
      this.http.put('http://localhost:7070/api/hotel', hotel).subscribe(value => {
        console.log(value);
        window.location.href = '/hotels-list';
      });
    }
  }

  delete(id: number): void {
    this.http.delete('http://localhost:7070//api/hotel/delete/' + id).subscribe(value => {
      console.log(value);
      window.location.href = '/hotels-list';
    });
  }
}
