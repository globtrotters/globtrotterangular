import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Continent} from "../model/continent";
import {Airport} from "../model/airport";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AirportService {

  constructor(private http: HttpClient) { }

  getAirports(): Observable<Airport[]> {
    return this.http.get<Airport[]>('http://localhost:7070/api/airport/all');
  }

  getAirport(id: number): Observable<Airport> {
    return this.http.get<Airport>('http://localhost:7070/api/airport/findById/' + id);
  }

  save(airport: Airport): void {
    if (airport.id == null) {
      this.http.post('http://localhost:7070/api/airport', airport).subscribe(value => {
        console.log(value);
        window.location.href = '/airports-list';
      });
    } else {
      this.http.put('http://localhost:7070/api/airport', airport).subscribe(value => {
        console.log(value);
        window.location.href = '/airports-list';
      });
    }
  }

  delete(id: number): void {
    this.http.delete('http://localhost:7070/api/airport/delete/' + id).subscribe(value => {
      console.log(value);
      window.location.href = '/airports-list';
    });
  }

}
