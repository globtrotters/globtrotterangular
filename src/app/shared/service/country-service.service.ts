import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Country} from '../model/country';

@Injectable({
  providedIn: 'root'
})
export class CountryServiceService {

  constructor(private http: HttpClient) {
  }

  getCountires(): Observable<Country[]> {
    return this.http.get<Country[]>('http://localhost:7070/api/country/all');
  }

  getCountry(id: number): Observable<Country> {
    return this.http.get<Country>('http://localhost:7070/api/country/findById/' + id);
  }

  save(country: Country): void {
    if (country.id == null) {
      this.http.post('http://localhost:7070/api/country', country).subscribe(value => {
        console.log(value);
        window.location.href = '/countries-list';
      });
    } else {
      this.http.put('http://localhost:7070/api/country', country).subscribe(value => {
        console.log(value);
        window.location.href = '/countries-list';
      });
    }
  }


  delete(id: number): void {
    this.http.delete('http://localhost:7070/api/country/delete/' + id).subscribe(value => {
      console.log(value);
      window.location.href = '/countries-list';
    });
  }
}
