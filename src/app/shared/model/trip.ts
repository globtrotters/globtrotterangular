export class Trip {
  id: number;
  airportFromId: number;
  airportToId: number;
  hotelId: number;
  cityId: number;
  //TODO jak ugryźć daty?
  departureDate: number;
  returnDate: number;
  countOfDays: number;
  //TODO co z tym enumem?
  type: string;
  priceForAdult: number;
  priceForChild: number;
  promotion: number;
  countOfPerson: number;
  description: string;
}
