import {Standard} from './standard';
import {City} from './city';

export class Hotel {
  id: number;
  name: string;
  //TODO jak rozkminić enum?
  standard: Standard;
  description: string;
  cityId: number;
}
