export enum Standard {
  ONESTAR = '*',
  TWOSTAR = '**',
  THREESTAR = '***',
  FOURSTAR = '****',
  FIVESTAR = '*****'
}
