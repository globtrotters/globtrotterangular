export enum FoodType {
  ONESTAR = 'BB',
  TWOSTAR = 'HB',
  THREESTAR = 'FB',
  FOURSTAR = 'AI'
}
