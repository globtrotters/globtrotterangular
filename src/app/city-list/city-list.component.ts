import {Component, OnInit} from '@angular/core';
import {City} from '../shared/model/city';
import {CityService} from '../shared/service/city.service';

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.css']
})
export class CityListComponent implements OnInit {

  private cities: City[];

  constructor(private cityService: CityService) {
  }

  ngOnInit() {
    this.cityService.getCities().subscribe(value => {
      console.log(value);
      this.cities = value;
    });
  }

  onDelete(id: number): void {
    this.cityService.delete(id);
  }

}
