import {Component, OnInit} from '@angular/core';
import {Country} from '../shared/model/country';
import {CountryServiceService} from '../shared/service/country-service.service';
import {Continent} from '../shared/model/continent';

@Component({
  selector: 'app-countires-list',
  templateUrl: './countries-list.component.html',
  styleUrls: ['./countries-list.component.css']
})
export class CountriesListComponent implements OnInit {

  private countries: Country[];
  private continent: Continent;

  constructor(private countryService: CountryServiceService) {
  }

  ngOnInit() {
    this.continent = new Continent();
    this.countryService.getCountires().subscribe(value => {
      console.log(value);
      this.countries = value;
    });
  }

  onDelete(id: number): void {
    this.countryService.delete(id);
  }

}
