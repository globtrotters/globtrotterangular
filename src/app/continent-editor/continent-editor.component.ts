import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Continent} from '../shared/model/continent';
import {ActivatedRoute, Router} from '@angular/router';
import {ContinentServiceService} from '../shared/service/conitnent-service';

@Component({
  selector: 'app-continent-editor',
  templateUrl: './continent-editor.component.html',
  styleUrls: ['./continent-editor.component.css']
})
export class ContinentEditorComponent implements OnInit {

  continent: Continent = new Continent();


  constructor(private location: Location, private route: ActivatedRoute, private router: Router,
              private continentService: ContinentServiceService) {
  }

  ngOnInit() {
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log('id:' + id);
    if (id) {
      this.continentService.getContinent(id).subscribe(value => {
        console.log(value);
        this.continent = value;
        this.continent.id = id;
      });
    }
  }

  onSave(): void {
    console.log(this.continent);
    this.continentService.save(this.continent);
  }
  // TODO przerobić resztę go backow tak jak ponizej
  goBack(): void {
    location.href = '/continent-list';
  }

}
