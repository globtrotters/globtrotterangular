import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {Country} from '../shared/model/country';
import {CountryServiceService} from '../shared/service/country-service.service';
import {Continent} from '../shared/model/continent';
import {ContinentServiceService} from '../shared/service/conitnent-service';

@Component({
  selector: 'app-countire-editor',
  templateUrl: './country-editor.component.html',
  styleUrls: ['./country-editor.component.css']
})
export class CountryEditorComponent implements OnInit {

  country: Country;
  continents: Continent[];
  continent: Continent;

  constructor(private location: Location, private route: ActivatedRoute, private router: Router,
              private countryService: CountryServiceService, private continentService: ContinentServiceService) {
  }

  ngOnInit() {
    this.country = new Country();
    this.continent = new Continent();
    this.continentService.getContinents().subscribe(value => {
      console.log(value);
      this.continents = value;
    });
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log('id:' + id);
    if (id) {
      this.countryService.getCountry(id).subscribe(value => {
        console.log(value);
        this.country = value;
        this.country.id = id;
      });
    }
  }

  onSave(): void {
    console.log(this.country);
    this.countryService.save(this.country);
  }

  goBack(): void {
    this.location.back();
  }

}
