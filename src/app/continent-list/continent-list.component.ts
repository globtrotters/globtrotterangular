import {Component, OnInit} from '@angular/core';
import {Continent} from '../shared/model/continent';
import {ContinentServiceService} from '../shared/service/conitnent-service';

@Component({
  selector: 'app-continent-list',
  templateUrl: './continent-list.component.html',
  styleUrls: ['./continent-list.component.css']
})
export class ContinentListComponent implements OnInit {

  private continents: Continent[];
  public continent: Continent = new Continent();

  constructor(private continentService: ContinentServiceService) {
  }

  ngOnInit() {
    this.continentService.getContinents().subscribe(value => {
      console.log(value);
      this.continents = value;
    });
  }

  onDelete(id: number): void {
    this.continentService.delete(id);
  }

}
