import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserIlstComponent} from './user-ilst/user-ilst.component';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AdminPageComponent} from './admin-page/admin-page.component';
import {ContinentEditorComponent} from './continent-editor/continent-editor.component';
import {ContinentListComponent} from './continent-list/continent-list.component';
import {CountriesListComponent} from './countries-list/countries-list.component';
import {CountryEditorComponent} from './country-editor/country-editor.component';
import {CityEditorComponent} from './city-editor/city-editor.component';
import {CityListComponent} from './city-list/city-list.component';
import {AirportEditorComponent} from './airport-editor/airport-editor.component';
import {AirportsListComponent} from './airports-list/airports-list.component';
import {HotelEditorComponent} from './hotel-editor/hotel-editor.component';
import {HotelsListComponent} from './hotels-list/hotels-list.component';
import {TripEditorComponent} from './trip-editor/trip-editor.component';
import {TripsListComponent} from './trips-list/trips-list.component';

/**
 * tablica routingu
 * definiujemy adres dla komponentu
 */
const routes: Routes = [
  {path: 'user-list', component: UserIlstComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'admin-page', component: AdminPageComponent},
  {path: 'continent-editor', component: ContinentEditorComponent},
  {path: 'continent-editor/:id', component: ContinentEditorComponent},
  {path: 'continent-list', component: ContinentListComponent},
  {path: 'countries-list', component: CountriesListComponent},
  {path: 'country-editor', component: CountryEditorComponent},
  {path: 'country-editor/:id', component: CountryEditorComponent},
  {path: 'city-list', component: CityListComponent},
  {path: 'city-editor', component: CityEditorComponent},
  {path: 'city-editor/:id', component: CityEditorComponent},
  {path: 'airport-editor', component: AirportEditorComponent},
  {path: 'airport-editor/:id', component: AirportEditorComponent},
  {path: 'airports-list', component: AirportsListComponent},
  {path: 'hotel-editor', component: HotelEditorComponent},
  {path: 'hotel-editor/:id', component: HotelEditorComponent},
  {path: 'hotels-list', component: HotelsListComponent},
  {path: 'trip-editor', component: TripEditorComponent},
  {path: 'trip-editor/:id', component: TripEditorComponent},
  {path: 'trips-list', component: TripsListComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
