import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {HotelService} from '../shared/service/hotel.service';
import {Hotel} from '../shared/model/hotel';

@Component({
  selector: 'app-hotel-editor',
  templateUrl: './hotel-editor.component.html',
  styleUrls: ['./hotel-editor.component.css']
})
export class HotelEditorComponent implements OnInit {

  hotel: Hotel;

  constructor(private location: Location, private route: ActivatedRoute, private router: Router,
              private hotelService: HotelService) {
  }

  ngOnInit() {
    this.hotel = new Hotel();
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log('id:' + id);
    if (id) {
      this.hotelService.getHotel(id).subscribe(value => {
        console.log(value);
        this.hotel = value;
        this.hotel.id = id;
      });
    }
  }

  onSave(): void {
    console.log(this.hotel);
    this.hotelService.save(this.hotel);
  }

  goBack(): void {
    location.href = '/hotel-list';
  }

}
