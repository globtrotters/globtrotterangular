import { Component, OnInit } from '@angular/core';
import {User} from '../shared/model/user';
import {UserServiceService} from '../shared/service/user-service.service';

@Component({
  selector: 'app-user-ilst',
  templateUrl: './user-ilst.component.html',
  styleUrls: ['./user-ilst.component.css']
})
export class UserIlstComponent implements OnInit {

  constructor(private userService: UserServiceService) { }

  users: User[] = new Array();

  ngOnInit() {
    this.userService.getAllUsers().subscribe(value => {
    this.users = value;
    console.log(this.users);
    });
  }

}
