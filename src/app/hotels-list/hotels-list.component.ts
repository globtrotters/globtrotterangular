import {Component, OnInit} from '@angular/core';
import {Hotel} from '../shared/model/hotel';
import {HotelService} from '../shared/service/hotel.service';

@Component({
  selector: 'app-hotels-list',
  templateUrl: './hotels-list.component.html',
  styleUrls: ['./hotels-list.component.css']
})
export class HotelsListComponent implements OnInit {

  private hotels: Hotel[];

  constructor(private hotelService: HotelService) {
  }

  ngOnInit() {
    this.hotelService.getHotels().subscribe(value => {
      console.log(value);
      this.hotels = value;
    });
  }

  onDelete(id: number): void {
    this.hotelService.delete(id);
  }
}
