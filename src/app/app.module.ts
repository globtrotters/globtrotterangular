import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserIlstComponent } from './user-ilst/user-ilst.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {UserServiceService} from './shared/service/user-service.service';
import {HttpClientModule} from '@angular/common/http';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { ContinentEditorComponent } from './continent-editor/continent-editor.component';
import {FormsModule} from '@angular/forms';
import { ContinentListComponent } from './continent-list/continent-list.component';
import { CountryEditorComponent } from './country-editor/country-editor.component';
import { CountriesListComponent } from './countries-list/countries-list.component';
import { CityListComponent } from './city-list/city-list.component';
import { CityEditorComponent } from './city-editor/city-editor.component';
import { AirportsListComponent } from './airports-list/airports-list.component';
import { AirportEditorComponent } from './airport-editor/airport-editor.component';
import { HotelsListComponent } from './hotels-list/hotels-list.component';
import { HotelEditorComponent } from './hotel-editor/hotel-editor.component';
import { TripsListComponent } from './trips-list/trips-list.component';
import { TripEditorComponent } from './trip-editor/trip-editor.component';

@NgModule({
  declarations: [
    AppComponent,
    UserIlstComponent,
    DashboardComponent,
    AdminPageComponent,
    ContinentEditorComponent,
    ContinentListComponent,
    CountryEditorComponent,
    CountriesListComponent,
    CityListComponent,
    CityEditorComponent,
    AirportsListComponent,
    AirportEditorComponent,
    HotelsListComponent,
    HotelEditorComponent,
    TripsListComponent,
    TripEditorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [UserServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
