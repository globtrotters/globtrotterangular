import {Component, OnInit} from '@angular/core';
import {City} from '../shared/model/city';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {CityService} from '../shared/service/city.service';
import {Country} from "../shared/model/country";
import {CountryServiceService} from "../shared/service/country-service.service";

@Component({
  selector: 'app-city-editor',
  templateUrl: './city-editor.component.html',
  styleUrls: ['./city-editor.component.css']
})
export class CityEditorComponent implements OnInit {

  city: City = new City();
  countries: Country[];

  constructor(private location: Location, private route: ActivatedRoute, private router: Router,
              private cityService: CityService, private countryService: CountryServiceService) {
  }

  ngOnInit() {
    this.countryService.getCountires().subscribe(value => {
      console.log(value);
      this.countries = value;
    });
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log('id:' + id);
    if (id) {
      this.cityService.getCity(id).subscribe(value => {
        console.log(value);
        this.city = value;
        this.city.id = id;
      });
    }
  }

  onSave(): void {
    console.log(this.city);
    this.cityService.save(this.city);
  }

  goBack(): void {
    this.location.back();
  }

}
