import {Component, OnInit} from '@angular/core';
import {Airport} from '../shared/model/airport';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {AirportService} from '../shared/service/airport.service';
import {Country} from '../shared/model/country';
import {CountryServiceService} from '../shared/service/country-service.service';
import {City} from '../shared/model/city';
import {CityService} from '../shared/service/city.service';

@Component({
  selector: 'app-airport-editor',
  templateUrl: './airport-editor.component.html',
  styleUrls: ['./airport-editor.component.css']
})
export class AirportEditorComponent implements OnInit {

  airport: Airport = new Airport();
  country: Country;
  countries: Country[];
  city: City;
  cities: City[];

  constructor(private location: Location, private route: ActivatedRoute, private router: Router,
              private airportService: AirportService, private countryService: CountryServiceService,
              private cityService: CityService) {
  }

  ngOnInit() {
    this.airport = new Airport();
    this.country = new Country();
    this.city = new City();
    this.countryService.getCountires().subscribe(value => {
      console.log(value);
      this.countries = value;
    });
    this.cityService.getCities().subscribe(value => {
      console.log(value);
      this.cities = value;
    });
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log('id:' + id);
    if (id) {
      this.airportService.getAirport(id).subscribe(value => {
        console.log(value);
        this.airport = value;
        this.airport.id = id;
      });
    }
  }

  onSave(): void {
    console.log(this.airport);
    this.airportService.save(this.airport);
  }

  goBack(): void {
    this.location.back();
  }

}
